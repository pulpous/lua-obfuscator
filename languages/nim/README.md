# nim

- `office - docs` - https://nim-lang.org/
- `office - code` - https://github.com/nim-lang/Nim


## GUI - nimx

- `github` - https://github.com/yglukhov/nimx
- `gitee` - https://gitee.com/mirrors/nimx/

nimx 是一个由Yegor Lukhov开发的 开源项目，<br>
它是一个用于构建图形用户界面（GUI）的应用程序框架，基于强大的 Nim 编程语言。<br>
Nimx提供了一套完整的工具集，使开发者能够创建出优雅且高效的跨平台应用，<br>
覆盖了Windows, macOS, Linux等主流操作系统。