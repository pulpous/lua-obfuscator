# 多编程语言

为了方便 `copy` 写了以下代码


## git submodul

```sh
  git submodule init

  # to path : languages/vb/vb6/vb6-lua-obfuscator
  git submodule add `<url.git>` languages/vb/vb6/vb6-lua-obfuscator


  # languages/arrdio
  # languages/c
  # languages/electron
  # languages/java
  # languages/lua
  # languages/nim
  # languages/nwjs
  # languages/objective-c
  # languages/python
  # languages/swift
  # languages/vbnet
```